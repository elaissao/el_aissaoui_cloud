// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDv6Lm42buRBdpVC2b_aU3N319OHrPEv6M",
    authDomain: "covidproject-541fc.firebaseapp.com",
    projectId: "covidproject-541fc",
    storageBucket: "covidproject-541fc.appspot.com",
    messagingSenderId: "1032139943376",
    appId: "1:1032139943376:web:d9e8f4c8472d761bcf5dd5",
    measurementId: "G-YX7HFDTCHE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
