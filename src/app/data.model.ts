export class Data_covid{
    deaths : number;
    countryName: string;
    countryCode : string;
    recover: number; 

    constructor (deaths: number, countryName: string, countryCode: string, recover : number){
        this.deaths = deaths;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.recover = recover;

    }
}