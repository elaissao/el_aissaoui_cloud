import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import {DatadisplayService} from '../datadisplay.service';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(public newsService : NewsService , 
    public datadisplayService: DatadisplayService) { }

  ngOnInit(): void {
  }

}
