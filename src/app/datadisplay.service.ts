import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


import { User } from './user.model';
import { News } from './news.model';
import { Data_covid } from './data.model';
import { CountryComponent } from './country/country.component';


@Injectable({
  providedIn: 'root'
})


export class DatadisplayService {


  apiUrl = 'https://api.covid19api.com';

  private user : User;
  private country : string; 
  public slug : string;
  private data : Data_covid;
  countryName : any;
  countryCode: any;



  constructor(private http: HttpClient, private router : Router, private firestore: AngularFirestore,
    private afAuth: AngularFireAuth) { }
    // Api to use for world data :  https://corona.lmao.ninja/v2/historical/all, deaths, recoveries, cases
    // daily : https://corona.lmao.ninja/v2/historical/all?lastdays=8 , 

   



    getDataSummary(){
      let newSummary = this.http.get("https://corona.lmao.ninja/v2/historical/all?lastdays=8");
    }
      

    getDataCountry(){
      var today = new Date();
      var last_date = today.getFullYear()+"." + (today.getMonth()).toString().padStart(2,"0") + "." + today.getDay().toString().padStart(2,"0");
      var eight_days_ago = new Date();
      eight_days_ago.setDate(eight_days_ago.getDate() - 8)
      var first_date = eight_days_ago.getFullYear()+"." + eight_days_ago.getMonth().toString().padStart(2,"0") + "." + eight_days_ago.getDay().toString().padStart(2,"0");
      
      return this.http.get(this.apiUrl+"total/country/"+this.countryCode+"?from="+first_date+"&to="+ last_date);
    }

    getCountry(){
      this.router.navigate(["country/"+])
      return this.country;
    }

    goWorldpage(){
      this.router.navigate(["signin"]);
      this.countryName = "world";
    }

    getDayOne(countryCode : string){
      let firstCollection = this.firestore.collection("firstDay");
      
      if(this.countryName == "world"){
        return this.http.get("https://corona.lmao.ninja/v2/historical/all");
      }
      else{
        return this.http.get(this.apiUrl+"total/dayone/country" + countryCode);
      }
    }
    async getCountryName(){
      var countrydata = this.http.get(this.apiUrl+"countries").subscribe((countryInfo:any) => {
        for ( let i = 0; i < Object.keys(countryInfo).length; i++){
          this.countryName = countryInfo[i]["Country"];
        }
      })
      return this.countryName
    }

    async getCountryCode(){
      var countrydata = this.http.get(this.apiUrl+"countries").subscribe((countryInfo:any) => {
        for ( let i = 0; i < Object.keys(countryInfo).length; i++){
          let countryCode = countryInfo[i]["Slug"];
        }
      })
      return this.countryCode
    }









}
