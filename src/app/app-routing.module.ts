import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { SecurePagesGuard } from './secure-pages.guard';
import {AuthGuard } from './auth.guard';

import { SigninComponent } from './signin/signin.component';
import { CountryComponent } from './country/country.component';
import { NewsComponent} from './news/news.component';



const routes: Routes = [
  { path: "signin", component: SigninComponent,
  canActivate: [SecurePagesGuard]},
  { path: "news", component: NewsComponent, canActivate : [AuthGuard]},
  { path: "Country", component: CountryComponent, 
  canActivate: [SecurePagesGuard]},
  { path: "", pathMatch: "full", redirectTo: "signin"},
  { path: "**", redirectTo: "signin"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
