import { Component, OnInit } from '@angular/core';

import { News } from '../news.model';
import { NewsService } from '../news.service';
import { User } from '../user.model';




@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

date : any;
description : string ;
uid : string;
user : User; 

  constructor(private newsService : NewsService) { }


  ngOnInit(): void {
  this.user = this.newsService.getUser();
  }

  addNews(){
    let news: News = {
      date : new Date(this.date),
      description : this.description,
      uid: this.user.uid,
    };
    this.newsService.addNews(news);
    this.date = undefined; 
    this.description = undefined; 
    this.uid = undefined;
  }

}
