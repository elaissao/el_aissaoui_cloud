export class News{
    date : any;
    description: string;
    uid: string;

    constructor(date: Date, description: string, uid : string){
        this.date = date;
        this.description = description;
        this.uid = uid;
    }
    
}