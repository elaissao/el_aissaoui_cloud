import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


import { User } from './user.model';
import { News } from './news.model';


@Injectable({
  providedIn: 'root'
})
export class NewsService {
  
   
  private user: User;
  public eligible: boolean;

  constructor(private afAuth: AngularFireAuth, 
    private router: Router, private firestore : AngularFirestore) { }

    async signInWithGoogle(){
      const credientals = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()); 
      this.user = {
         uid: credientals.user.uid,
         displayName: credientals.user.displayName,
         email: credientals.user.email
       };
       localStorage.setItem("user", JSON.stringify(this.user));
       this.updateUserData();
       this.router.navigate(["news"]);

       let currentUser: User = {uid:'', email : '', displayName :''}
       //user allowed  ? 
       this.firestore.collection("eligible").doc(currentUser.uid).get().subscribe(doc => {
         if(doc.exists){
           console.log("Welcome ! You can add news !")
           this.firestore.collection("users").doc(currentUser.uid).set(this.user, {merge: true});
           this.eligible = true;
         } else{
           console.log("User not eligible to add news");
           this.eligible = false;
         }
       })
     }


     private updateUserData(){
      this.firestore.collection("users").doc(this.user.uid).set({
        uid: this.user.uid,
        displayName: this.user.displayName,
        email: this.user.email
      }, { merge: true});
    }

    getUser(){
      if(this.user == null && this.userSignedIn()){
        this.user = JSON.parse(localStorage.getItem("user"));
      }
      return this.user;
    }

    userSignedIn(): boolean{
      return JSON.parse(localStorage.getItem("user")) != null;
    }

    addNews( news: News){
      this.firestore.collection("users").doc(this.user.uid).collection("News").add(news);
      this.firestore.collection("users").doc(this.user.uid).collection("News").doc("news").set({
        author : news.uid,
        description : news.description,
        data: news.date
      }, {merge: true});
    }
    

    getNews(){
      this.router.navigate(["addNews"]);
      return this.firestore.collection("news", ref => ref.orderBy('date')).valueChanges();
    }

    allowedUsers(){
      return this.eligible;
    }


    signOut(){
      this.afAuth.signOut();
      localStorage.removeItem("user");
      this.user = null;
      this.router.navigate(["signin"]);
    }    

    
}
