import { Component, OnInit } from '@angular/core';

import { User } from '../user.model';
import { NewsService } from '../news.service' ;
import { News } from '../news.model';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  user: User; 
  news : News[];

  constructor(public newsService : NewsService) { }

  ngOnInit(): void {
    this.user = this.newsService.getUser();
    this.newsService.getNews().subscribe((news) => {
      this.news = news as News[];
    });
  }
}
