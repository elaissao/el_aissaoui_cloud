import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';


import { DatadisplayService } from './datadisplay.service';
import { NewsService } from './news.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private newsService : NewsService, private datadisplayService: DatadisplayService,
    private router: Router){};



  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.newsService.userSignedIn()){
      this.router.navigate(["signin"]);
    }
    if(!this.newsService.allowedUsers()){
      return true;
  }else{
    return false;
  }
}
}
